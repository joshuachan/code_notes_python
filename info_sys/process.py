# process.py: 简单信息管理系统
# function: 实现记录添加/查找/删除/排序/导入/导出
# author: Joshua Chan <joshuachan@yeah.net> 2012/04/08
import pickle
import menu

class Record:
    """定义信息记录的类"""
    def __init__(self, id, name, gender, dept):
        self.id = id
        self.name = name
        self.gender = gender
        self.dept = dept

    def show_title():
        print('{0:6}{1:12}{2:10}{3:12}'.format('ID', 'Name', 'Gender', 'Dept'))
        print('-'*40)

    def get_id(self):
        return self.id

    def show_record(self):
        print('{0:6}{1:12}{2:10}{3:12}'.format(
                self.id, self.name, self.gender, self.dept))

def add_record(info_list, path):
    """由用户输入数据生成记录并添加至列表"""
    while True:
        print('Please input information or just press enter to return.')
        s = input('Format: ID, Name, Gender, Dept\n')
        if s == '':
            menu.show_main_menu()
            menu.select_main_menu(info_list, path)
            break

        l = s.split(',')
        if len(l) == 4:
            l = [x.strip() for x in l]
            info_list.append(Record(l[0], l[1], l[2], l[3]))
            input('New record was added, press enter to continue... ')
        else:
            input('Wrong format, press enter to continue... ')

def info_query(info_list, path):
    """遍历所有记录"""
    print()
    Record.show_title()
    for x in info_list:
        x.show_record()
    print('Total:', len(info_list))
    input('\nPress enter to continue... ')

    menu.show_main_menu()
    menu.select_main_menu(info_list, path)

def sort_record(info_list, path):
    """以ID为基准排序"""
    info_list.sort(key = Record.get_id)
    print('\nAll records were sorted.')
    input('Press enter to continue... ')

    menu.show_main_menu()
    menu.select_main_menu(info_list, path)

def search_record(info_list, path):
    """根据输入ID查找记录"""
    id = input('\nPlease input ID number: ')
    for x in info_list:
        if x.id == id:
            print()
            Record.show_title()
            x.show_record()
            input('\nRecord was found, press enter to continue... ')
            break
    else:
        input('Record not found, press enter to continue... ')

    menu.show_main_menu()
    menu.select_main_menu(info_list, path)

def remove_record(info_list, path):
    """根据输入ID查找并删除记录"""
    id = input('\nPlease input ID number: ')
    for x in info_list:
        if x.id == id:
            print()
            Record.show_title()
            x.show_record()
            c = input('\nRecord was found, REMOVE it please input y,\n'
                      'or just press enter to cancel: ')
            if c == 'y' or c == 'Y':
                info_list.remove(x)
                input('\nRecord was removed, press enter to continue... ')
            break
    else:
        input('\nRecord not found, press enter to continue... ')

    menu.show_main_menu()
    menu.select_main_menu(info_list, path)

def import_data(path):
    """从文件中导入记录列表"""
    try:
        f = open(path, 'rb')
        info_list = pickle.load(f)
        f.close()
    except IOError:
        print('Import data failed, file not exist.')
        info_list = []
    return info_list

def export_data(info_list, path):
    """将记录列表存入文件"""
    f = open(path, 'wb')
    pickle.dump(info_list, f)
    f.close()
    
def save_now(info_list, path):
    """立即将记录列表存入文件"""
    export_data(info_list, path)
    input('\nAll records were saved, press enter to continue... ')

    menu.show_main_menu()
    menu.select_main_menu(info_list, path)
