# menu.py: 简单信息管理系统
# function: 实现记录添加/查找/删除/排序/导入/导出
# author: Joshua Chan <joshuachan@yeah.net> 2012/04/08
from process import *

def show_main_menu():
    """显示主菜单"""
    print('\n*** Information Management System ***')
    print('=====================================')
    print('  [1] Add Record')
    print('  [2] Information Query')
    print('  [3] Sort by ID')
    print('  [4] Search by ID')
    print('  [5] Remove by ID')
    print('  [6] Save Now')
    print('  [0] Exit\n')

def select_main_menu(info_list, path):
    """主菜单功能跳转"""
    while True:
        c = input('Please input choice: ')
        if c == '1':
            add_record(info_list, path)
            break
        elif c == '2':
            info_query(info_list, path)
            break
        elif c == '3':
            sort_record(info_list, path)
            break
        elif c == '4':
            search_record(info_list, path)
            break
        elif c == '5':
            remove_record(info_list, path)
            break
        elif c == '6':
            save_now(info_list, path)
            break
        elif c == '0':
            return
        else:
            print('Invalid selection.')

