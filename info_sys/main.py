# main.py: 简单信息管理系统
# function: 实现记录添加/查找/删除/排序/导入/导出
# author: Joshua Chan <joshuachan@yeah.net> 2012/04/08
from menu import *
from process import *

info_list = []
path = 'data.txt'

info_list = import_data(path)
show_main_menu()
select_main_menu(info_list, path)
export_data(info_list, path)
