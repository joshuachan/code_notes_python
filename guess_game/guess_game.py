# 猜数字游戏

from random import randint

class Guess:
    def __init__(self):
        """类的初始化方法"""
        self.title()
        while True:
            number = self.generate()
            self.guess(number)
            self.over()

    def title(self):
        """显示标题"""
        print()
        print("    猜数字游戏")
        print("=" * 18)

    def generate(self):
        """数字的生成"""
        print()
        number = randint(0, 100)
        print("一个神秘的数字诞生了，它位于0到100之间，来猜猜看吧！")
        print()
        return number

    def guess(self, number):
        """猜数字方法"""
        while True:
            i = input("请输入一个0至100之间的数字：")
            i = int(i)
            print()
            if i > number:
                print("输入的数字大了一点，请再猜一次看。")
            elif i < number:
                print("输入的数字小了一点，请再猜一次看。")
            else:
                print("真聪明，猜对了！")
                break

    def over(self):
        """一局结束后的提示"""
        print()
        print("要再来一局吗？")
        choice = input("输入'N'退出，或者直接按回车继续：")
        if choice == 'N' or choice == 'n':
            exit()

# 若是直接执行, 则会进入到下面语句
if __name__ == "__main__":
    guess = Guess()